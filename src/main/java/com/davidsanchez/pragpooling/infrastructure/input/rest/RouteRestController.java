package com.davidsanchez.pragpooling.infrastructure.input.rest;

import com.davidsanchez.pragpooling.application.dto.response.RouteResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.UserResponseDto;
import com.davidsanchez.pragpooling.application.handler.IRouteHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/route")
@RequiredArgsConstructor
public class RouteRestController {

    private final IRouteHandler routeHandler;

    @Operation(summary = "Get all routes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All routes returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = RouteResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/")
    public ResponseEntity<List<RouteResponseDto>> getAllRoutes() {
        return ResponseEntity.ok(routeHandler.getAllRoutes());
    }

    @Operation(summary = "Get a user by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Route found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "Route not found", content = @Content)
    })

    @GetMapping("/{routeId}")
    public ResponseEntity<RouteResponseDto> getRouteById(@Parameter(description = "Id of the route to be returned")
                                                         @PathVariable Long routeId) {
        return ResponseEntity.ok(routeHandler.getRouteById(routeId));
    }

    @Operation(summary = "Delete a route by their id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Route deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "Route not found", content = @Content)
    })
    @DeleteMapping("/{routeId}")
    public ResponseEntity<StringResponseDto> deleteRoute(@PathVariable Long routeId) {
        return new ResponseEntity<>(routeHandler.deleteRoute(routeId), HttpStatus.OK);
    }
}