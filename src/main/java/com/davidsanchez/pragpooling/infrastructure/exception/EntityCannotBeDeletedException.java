package com.davidsanchez.pragpooling.infrastructure.exception;

public class EntityCannotBeDeletedException extends RuntimeException {
    public EntityCannotBeDeletedException() {
        super();
    }
}
