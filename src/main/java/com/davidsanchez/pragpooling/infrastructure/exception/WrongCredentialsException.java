package com.davidsanchez.pragpooling.infrastructure.exception;

public class WrongCredentialsException extends RuntimeException {
    public WrongCredentialsException() {
        super();
    }
}
