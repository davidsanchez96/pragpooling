package com.davidsanchez.pragpooling.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "user_id", nullable = false)
    private Long id;

    @Column(name = "first_name", length = 50)
    private String firstName;

    @Column(name = "last_name", length = 50)
    private String lastName;

    @Column(length = 50)
    private String email;

    @Column(length = 50)
    private String address;

    @Column(name = "phone_number", length = 50)
    private String phoneNumber;

    @Column(length = 20)
    private String password;

    @Column(length = 100)
    private String username;

    @Column(name = "ride_id")
    private Long rideId = null;

    @ManyToOne
    @JoinColumn(name = "ride_id", referencedColumnName = "ride_id", insertable=false, updatable=false)
    private RideEntity rideByRideId;
}
