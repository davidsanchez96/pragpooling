package com.davidsanchez.pragpooling.infrastructure.out.jpa.repository;

import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.PathEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPathRepository extends JpaRepository<PathEntity, Long> {
}