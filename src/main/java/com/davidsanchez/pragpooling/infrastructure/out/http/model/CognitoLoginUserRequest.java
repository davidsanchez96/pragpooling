package com.davidsanchez.pragpooling.infrastructure.out.http.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CognitoLoginUserRequest {
    private String name;
    private String password;
}
