package com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper;

import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RideEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IRideEntityMapper {

    RideEntity toEntity(Ride ride);

    List<RideEntity> toEntityList(List<Ride> rideList);

    Ride toRide(RideEntity rideEntity);

    List<Ride> toRideList(List<RideEntity> rideEntityList);
}