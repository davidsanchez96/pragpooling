package com.davidsanchez.pragpooling.infrastructure.out.jpa.adapter;

import com.davidsanchez.pragpooling.domain.model.Route;
import com.davidsanchez.pragpooling.domain.spi.IRoutePersistencePort;
import com.davidsanchez.pragpooling.infrastructure.exception.NoDataFoundException;
import com.davidsanchez.pragpooling.infrastructure.exception.RouteNotFoundException;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RouteEntity;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper.IRouteEntityMapper;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.repository.IRouteRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class RouteJpaAdapter implements IRoutePersistencePort {

    private final IRouteRepository routeRepository;
    private final IRouteEntityMapper routeEntityMapper;

    @Override
    public Route saveRoute(Route route) {
        RouteEntity routeEntity = routeRepository.save(routeEntityMapper.toEntity(route));
        return routeEntityMapper.toRoute(routeEntity);
    }

    @Override
    public List<Route> getAllRoutes() {
        List<RouteEntity> routeEntityList = routeRepository.findAll();
        if (routeEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return routeEntityMapper.toRouteList(routeEntityList);
    }

    @Override
    public Route getRouteById(Long routeId) {
        Optional<RouteEntity> routeEntity = routeRepository.findById(routeId);
        if (routeEntity.isEmpty()) {
            throw new RouteNotFoundException();
        }
        return routeEntityMapper.toRoute(routeEntity.get());
    }

    @Override
    public List<Route> getAllRoutesByDriverId(Long driverId) {
        List<RouteEntity> allByDriverId = routeRepository.findAllByDriverId(driverId);
        return routeEntityMapper.toRouteList(allByDriverId);
    }

    @Override
    public void deleteRoute(Long routeId) {
        Optional<RouteEntity> routeEntity = routeRepository.findById(routeId);
        if (routeEntity.isEmpty()) {
            throw new RouteNotFoundException();
        }
        routeRepository.deleteById(routeId);
    }


}