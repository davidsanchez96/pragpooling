package com.davidsanchez.pragpooling.infrastructure.out.jpa.repository;

import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.RideEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface IRideRepository extends JpaRepository<RideEntity, Long>, JpaSpecificationExecutor<RideEntity> {
    @Modifying
    @Query(value = "UPDATE ride SET available_seats = LAST_INSERT_ID(available_seats - 1) WHERE ride_id = :rideId", nativeQuery = true)
    Integer decrementAvailableSeats(Long rideId);

    @Modifying
    @Query(value = "UPDATE ride SET available_seats = LAST_INSERT_ID(available_seats + 1) WHERE ride_id = :rideId", nativeQuery = true)
    Integer incrementAvailableSeats(Long rideId);

    @Modifying
    @Query(value = "UPDATE ride SET stops = LAST_INSERT_ID(stops - 1) WHERE ride_id = :rideId", nativeQuery = true)
    Integer decrementStops(Long rideId);

    @Modifying
    @Query(value = "UPDATE ride SET stops = LAST_INSERT_ID(stops + 1) WHERE ride_id = :rideId", nativeQuery = true)
    Integer incrementStops(Long rideId);
}