package com.davidsanchez.pragpooling.infrastructure.out.jpa.mapper;

import com.davidsanchez.pragpooling.domain.model.Path;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.PathEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IPathEntityMapper {

    PathEntity toEntity(Path user);

    Path toPath(PathEntity userEntity);

    List<Path> toPathList(List<PathEntity> pathEntityList);

    List<PathEntity> toEntityList(List<Path> pathList);
}