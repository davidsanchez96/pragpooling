package com.davidsanchez.pragpooling.infrastructure.out.http.service;

import com.davidsanchez.pragpooling.infrastructure.exception.CognitoException;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoLoginUserBodyResponse;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoLoginUserRequest;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoLoginUserResponse;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoSignupUserBodyResponse;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoSignupUserRequest;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoSignupUserResponse;
import com.davidsanchez.pragpooling.infrastructure.out.http.model.CognitoTokenResponse;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class CognitoService implements ICognitoService {

    private static final String BASE_URL = "https://k6gwndgg8j.execute-api.us-east-1.amazonaws.com/pdn/bootcamppowerup";

    private MultiValueMap<String, String> getHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        headers.setAll(map);
        return headers;
    }

    public String signup(CognitoSignupUserRequest signupUserRequest) {
        HttpEntity<CognitoSignupUserRequest> request = new HttpEntity<>(signupUserRequest, getHeaders());
        String url = BASE_URL + "/Signup";
        ResponseEntity<CognitoSignupUserResponse> responseEntity = new RestTemplate()
                .postForEntity(url, request, CognitoSignupUserResponse.class);

        CognitoSignupUserResponse signupUserResponse = responseEntity.getBody();
        if (signupUserResponse == null) {
            throw new CognitoException();
        }

        Gson gson = new Gson();
        CognitoSignupUserBodyResponse userData = gson.fromJson(
                signupUserResponse.getBody(),
                CognitoSignupUserBodyResponse.class
        );

        if (StringUtils.isEmpty(userData.getData())) {
            throw new CognitoException();
        }
        return userData.getData();
    }

    public CognitoTokenResponse login(CognitoLoginUserRequest loginUserRequest) {
        HttpEntity<CognitoLoginUserRequest> request = new HttpEntity<>(loginUserRequest, getHeaders());
        String url = BASE_URL + "/Login";

        ResponseEntity<CognitoLoginUserResponse> responseEntity = new RestTemplate()
                .postForEntity(url, request, CognitoLoginUserResponse.class);

        CognitoLoginUserResponse loginUserResponse = responseEntity.getBody();

        if (loginUserResponse == null) {
            throw new CognitoException();
        }

        Gson gson = new Gson();
        CognitoLoginUserBodyResponse userBodyResponse = gson.fromJson(
                loginUserResponse.getBody(),
                CognitoLoginUserBodyResponse.class
        );

        if (userBodyResponse.getTokenResponse() == null) {
            throw new CognitoException();
        }

        return userBodyResponse.getTokenResponse();
    }
}
