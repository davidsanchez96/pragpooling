package com.davidsanchez.pragpooling.application.handler.impl;

import com.davidsanchez.pragpooling.application.dto.request.RouteRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.RouteResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.handler.IDriverHandler;
import com.davidsanchez.pragpooling.application.mapper.ICurrentUserRequestMapper;
import com.davidsanchez.pragpooling.application.mapper.IMessageResponseMapper;
import com.davidsanchez.pragpooling.application.mapper.IRouteRequestMapper;
import com.davidsanchez.pragpooling.application.mapper.IRouteResponseMapper;
import com.davidsanchez.pragpooling.domain.api.IDriverServicePort;
import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.SaveRoute;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class DriverHandler implements IDriverHandler {

    private final IDriverServicePort driverServicePort;
    private final IRouteRequestMapper routeRequestMapper;
    private final IRouteResponseMapper routeResponseMapper;
    private final IMessageResponseMapper stringResponseMapper;
    private final ICurrentUserRequestMapper currentUserRequestMapper;

    @Override
    public StringResponseDto saveRoute(RouteRequestDto routeRequestDto, UserAuthPrincipal user) {
        SaveRoute saveRoute = routeRequestMapper.toCreateRoute(routeRequestDto);
        CurrentUser currentUser = currentUserRequestMapper.toCurrentUser(user);

        MessageResponse messageResponse = driverServicePort.saveRoute(saveRoute, currentUser);
        return stringResponseMapper.toResponse(messageResponse);
    }

    @Override
    public List<RouteResponseDto> getAllRoutes(UserAuthPrincipal user) {
        CurrentUser currentUser = currentUserRequestMapper.toCurrentUser(user);
        return routeResponseMapper.toResponseList(driverServicePort.getAllRoutes(currentUser));
    }
}