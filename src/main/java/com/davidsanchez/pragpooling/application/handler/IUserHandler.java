package com.davidsanchez.pragpooling.application.handler;

import com.davidsanchez.pragpooling.application.dto.response.StringResponseDto;
import com.davidsanchez.pragpooling.application.dto.request.UserLoginRequestDto;
import com.davidsanchez.pragpooling.application.dto.response.UserLoginResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.UserResponseDto;
import com.davidsanchez.pragpooling.application.dto.request.UserSignupRequestDto;

import java.util.List;

public interface IUserHandler {

    UserLoginResponseDto loginUser(UserLoginRequestDto userLoginRequestDto);

    StringResponseDto saveUser(UserSignupRequestDto userSignupRequestDto);

    List<UserResponseDto> getAllUsers();

    UserResponseDto getUserByEmail(String email);

    void deleteUser(Long userId);
}