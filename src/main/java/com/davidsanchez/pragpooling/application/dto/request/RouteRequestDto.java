package com.davidsanchez.pragpooling.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class RouteRequestDto {
    private Integer seats;
    private List<Timestamp> rides;
    private List<Long> neighborhoods;
    private String description;
    private String meetingPoint;
}
