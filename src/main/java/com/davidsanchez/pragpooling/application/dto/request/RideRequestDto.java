package com.davidsanchez.pragpooling.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class RideRequestDto {
    private Long neighborhoodOriginId;
    private Long neighborhoodDestinationId;
    private Timestamp date;
}
