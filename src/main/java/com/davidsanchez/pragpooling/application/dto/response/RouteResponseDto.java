package com.davidsanchez.pragpooling.application.dto.response;

import com.davidsanchez.pragpooling.domain.model.value_object.PathSlim;
import com.davidsanchez.pragpooling.domain.model.value_object.RideSlim;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class RouteResponseDto {
    private Long id;
    private Integer totalPaths;
    private Long driverId;
    private Collection<PathSlim> pathsByRouteId;
    private Collection<RideSlim> ridesByRouteId;
}
