package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.request.UserLoginRequestDto;
import com.davidsanchez.pragpooling.application.dto.request.UserSignupRequestDto;
import com.davidsanchez.pragpooling.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IUserRequestMapper {

    User toUser(UserSignupRequestDto userSignupRequestDto);

    User toUser(UserLoginRequestDto userLoginRequestDto);
}
