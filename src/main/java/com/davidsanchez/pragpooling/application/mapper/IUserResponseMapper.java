package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.application.dto.response.UserLoginResponseDto;
import com.davidsanchez.pragpooling.application.dto.response.UserResponseDto;
import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IUserResponseMapper {

    UserResponseDto toResponse(User user);

    UserLoginResponseDto toResponse(CognitoToken cognitoToken);

    List<UserResponseDto> toResponseList(List<User> userList);
}
