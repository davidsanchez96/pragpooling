package com.davidsanchez.pragpooling.application.mapper;

import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.infrastructure.security.UserAuthPrincipal;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface ICurrentUserRequestMapper {

    CurrentUser toCurrentUser(UserAuthPrincipal authPrincipal);
}
