package com.davidsanchez.pragpooling.domain.spi;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;

import java.util.List;

public interface INeighborhoodPersistencePort {

    Neighborhood saveNeighborhood(Neighborhood neighborhood);

    List<Neighborhood> getAllNeighborhoods();

    void deleteNeighborhood(Long neighborhoodId);

    void validateIds(List<Long> neighborhoodIds);

    Neighborhood findNeighborhoodById(Long neighborhoodId);
}