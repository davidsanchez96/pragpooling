package com.davidsanchez.pragpooling.domain.spi;

import com.davidsanchez.pragpooling.domain.model.Route;

import java.util.List;

public interface IRoutePersistencePort {

    Route saveRoute(Route route);

    List<Route> getAllRoutes();

    Route getRouteById(Long routeId);

    List<Route> getAllRoutesByDriverId(Long driverId);

    void deleteRoute(Long routeId);

}