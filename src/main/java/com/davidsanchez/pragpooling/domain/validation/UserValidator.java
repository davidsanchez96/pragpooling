package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.model.User;

import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireNonBlank;
import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireNonNull;
import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireValidEmail;
import static com.davidsanchez.pragpooling.domain.validation.ValidationUtils.requireValidPassword;

public class UserValidator {

    private UserValidator() {
    }

    public static void validateSaveUser(final User user) {
        requireNonNull(user, "User");
        requireNonBlank(user.getFirstName(), "firstName");
        requireNonBlank(user.getLastName(), "lastName");
        requireValidEmail(user.getEmail(), "email");
        requireNonBlank(user.getAddress(), "address");
        requireNonBlank(user.getPhoneNumber(), "phoneNumber");
        requireValidPassword(user.getPassword(), "password");
    }

    public static void validateLoginUser(User user) {
        requireNonNull(user, "User");
        requireValidEmail(user.getEmail(), "email");
        requireNonBlank(user.getPassword(), "password");
    }
}
