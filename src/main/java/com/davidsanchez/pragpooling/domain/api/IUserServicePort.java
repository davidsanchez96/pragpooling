package com.davidsanchez.pragpooling.domain.api;

import com.davidsanchez.pragpooling.domain.model.CognitoToken;
import com.davidsanchez.pragpooling.domain.model.MessageResponse;
import com.davidsanchez.pragpooling.domain.model.User;

import java.util.List;

public interface IUserServicePort {

    CognitoToken loginUser(User user);

    MessageResponse saveUser(User user);

    List<User> getAllUsers();

    User getUserByEmail(String email);

    void deleteUser(Long userId);
}