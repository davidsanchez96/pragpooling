package com.davidsanchez.pragpooling.domain.model;

import com.davidsanchez.pragpooling.domain.model.value_object.PathSlim;

import java.util.Collection;


public class Neighborhood {
    private Long id;
    private String name;
    private Collection<PathSlim> pathsByNeighborhoodId;

    public Neighborhood() {
    }

    public Neighborhood(Long id, String name, Collection<PathSlim> pathsByNeighborhoodId) {
        this.id = id;
        this.name = name;
        this.pathsByNeighborhoodId = pathsByNeighborhoodId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<PathSlim> getPathsByNeighborhoodId() {
        return pathsByNeighborhoodId;
    }

    public void setPathsByNeighborhoodId(Collection<PathSlim> pathsByNeighborhoodId) {
        this.pathsByNeighborhoodId = pathsByNeighborhoodId;
    }
}
