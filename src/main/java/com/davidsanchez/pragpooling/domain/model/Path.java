package com.davidsanchez.pragpooling.domain.model;

public class Path {
    private Long id;
    private Integer position;
    private Long neighborhoodId;
    private Long routeId;
    private Neighborhood neighborhoodByNeighborhoodId;
    private Route routeByRouteId;

    public Path() {
    }

    public Path(Long id, Integer position, Long neighborhoodId, Long routeId, Neighborhood neighborhoodByNeighborhoodId, Route routeByRouteId) {
        this.id = id;
        this.position = position;
        this.neighborhoodId = neighborhoodId;
        this.routeId = routeId;
        this.neighborhoodByNeighborhoodId = neighborhoodByNeighborhoodId;
        this.routeByRouteId = routeByRouteId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Long getNeighborhoodId() {
        return neighborhoodId;
    }

    public void setNeighborhoodId(Long neighborhoodId) {
        this.neighborhoodId = neighborhoodId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Neighborhood getNeighborhoodByNeighborhoodId() {
        return neighborhoodByNeighborhoodId;
    }

    public void setNeighborhoodByNeighborhoodId(Neighborhood neighborhoodByNeighborhoodId) {
        this.neighborhoodByNeighborhoodId = neighborhoodByNeighborhoodId;
    }

    public Route getRouteByRouteId() {
        return routeByRouteId;
    }

    public void setRouteByRouteId(Route routeByRouteId) {
        this.routeByRouteId = routeByRouteId;
    }
}
