package com.davidsanchez.pragpooling.domain.model.value_object;

public class NeighborhoodSlim {
    private Long id;
    private String name;

    public NeighborhoodSlim() {
    }

    public NeighborhoodSlim(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
