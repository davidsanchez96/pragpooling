package com.davidsanchez.pragpooling.domain.exception;

public class DuplicateNeighborhoodException extends RuntimeException {
    public DuplicateNeighborhoodException() {
        super();
    }
}
