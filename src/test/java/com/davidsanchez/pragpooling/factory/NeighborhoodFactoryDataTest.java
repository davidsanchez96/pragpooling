package com.davidsanchez.pragpooling.factory;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.infrastructure.out.jpa.entity.NeighborhoodEntity;

public class NeighborhoodFactoryDataTest {

    private NeighborhoodFactoryDataTest() {

    }

    public static Neighborhood getNeighborhood() {
        Neighborhood neighborhood = new Neighborhood();
        neighborhood.setId(1L);
        neighborhood.setName("Marc Drive");
        return neighborhood;
    }

    public static NeighborhoodEntity getNeighborhoodEntity() {
        NeighborhoodEntity neighborhood = new NeighborhoodEntity();
        neighborhood.setId(1L);
        neighborhood.setName("Marc Drive");
        return neighborhood;
    }
}
