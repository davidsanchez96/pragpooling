package com.davidsanchez.pragpooling.domain.validation;

import com.davidsanchez.pragpooling.domain.exception.DomainException;
import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.factory.NeighborhoodFactoryDataTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class NeighborhoodValidatorTest {

    @Test
    void when_nameIsBlank_expect_DomainException() {
        // Given
        Neighborhood neighborhood = NeighborhoodFactoryDataTest.getNeighborhood();
        neighborhood.setName("");

        // When
        Throwable throwable = Assertions.catchThrowable(() -> NeighborhoodValidator.validateNeighborhood(neighborhood));

        // Then
        Assertions.assertThat(throwable).isExactlyInstanceOf(DomainException.class);
    }

    @Test
    void when_nameIsNotBlank_shouldNot_throwAnything() {
        // Given
        Neighborhood neighborhood = NeighborhoodFactoryDataTest.getNeighborhood();
        neighborhood.setName("Tara");

        // When
        Throwable throwable = Assertions.catchThrowable(() -> NeighborhoodValidator.validateNeighborhood(neighborhood));

        // Then
        Assertions.assertThat(throwable).isNull();
    }


}