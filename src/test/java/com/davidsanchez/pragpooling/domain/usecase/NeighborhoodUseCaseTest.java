package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.model.Neighborhood;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.factory.NeighborhoodFactoryDataTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class NeighborhoodUseCaseTest {

    @InjectMocks
    NeighborhoodUseCase neighborhoodUseCase;

    @Mock
    INeighborhoodPersistencePort neighborhoodPersistencePort;

    @Test
    void when_saveNeighborhood_expect_persistencePort_saveNeighborhood() {
        // Given
        Neighborhood neighborhood = NeighborhoodFactoryDataTest.getNeighborhood();

        // When
        when(neighborhoodPersistencePort.saveNeighborhood(neighborhood)).thenReturn(neighborhood);
        Throwable throwable = catchThrowable(() -> neighborhoodUseCase.saveNeighborhood(neighborhood));

        // Then
        assertThat(throwable).isNull();
    }

    @Test
    void when_getAllNeighborhoods_expect_persistencePort_getAllNeighborhoods() {
        // When
        neighborhoodUseCase.getAllNeighborhoods();

        // Then
        verify(neighborhoodPersistencePort).getAllNeighborhoods();
    }

    @Test
    void when_deleteNeighborhood_expect_persistencePort_deleteNeighborhood() {
        // When
        neighborhoodUseCase.deleteNeighborhood(anyLong());

        // Then
        verify(neighborhoodPersistencePort).deleteNeighborhood(anyLong());
    }
}