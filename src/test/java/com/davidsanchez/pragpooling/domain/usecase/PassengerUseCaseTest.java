package com.davidsanchez.pragpooling.domain.usecase;

import com.davidsanchez.pragpooling.domain.exception.NoMoreSeatsAvailableException;
import com.davidsanchez.pragpooling.domain.exception.PassengerCantBeTheDriverException;
import com.davidsanchez.pragpooling.domain.exception.PassengerHasNoRideException;
import com.davidsanchez.pragpooling.domain.model.AvailableRide;
import com.davidsanchez.pragpooling.domain.model.CurrentUser;
import com.davidsanchez.pragpooling.domain.model.Ride;
import com.davidsanchez.pragpooling.domain.model.User;
import com.davidsanchez.pragpooling.domain.spi.INeighborhoodPersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IRidePersistencePort;
import com.davidsanchez.pragpooling.domain.spi.IUserPersistencePort;
import com.davidsanchez.pragpooling.factory.RideFactoryDataTest;
import com.davidsanchez.pragpooling.factory.UserFactoryDataTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class PassengerUseCaseTest {

    @InjectMocks
    PassengerUseCase passengerUseCase;

    @Mock
    IUserPersistencePort userPersistencePort;

    @Mock
    IRidePersistencePort ridePersistencePort;

    @Mock
    INeighborhoodPersistencePort neighborhoodPersistencePort;


    @Test
    void should_join_ride_given_valid_request() {
        // Given
        Ride ride = RideFactoryDataTest.getRide();
        User passenger = UserFactoryDataTest.getUser();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        Long rideId = 1L;
        passenger.setId(1L);
        ride.getRouteByRouteId().setDriverId(2L);

        // When
        when(ridePersistencePort.getRideById(rideId)).thenReturn(ride);
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(passenger);

        passengerUseCase.joinRide(rideId, currentUser);

        // Then
        verify(ridePersistencePort).decrementAvailableSeats(rideId);
        verify(userPersistencePort).joinRide(passenger, rideId);
        verify(ridePersistencePort).incrementStops(rideId);
    }

    @Test
    void should_throw_exception_given_driver_and_passenger_has_same_id() {
        // Given
        Ride ride = RideFactoryDataTest.getRide();
        User passenger = UserFactoryDataTest.getUser();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        Long rideId = 1L;
        passenger.setId(1L);
        ride.getRouteByRouteId().setDriverId(1L);

        // When
        when(ridePersistencePort.getRideById(rideId)).thenReturn(ride);
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(passenger);

        Throwable throwable = catchThrowable(() -> passengerUseCase.joinRide(rideId, currentUser));

        // Then
        assertThat(throwable).isExactlyInstanceOf(PassengerCantBeTheDriverException.class);
    }

    @Test
    void should_throw_exception_no_seats_available() {
        // Given
        Ride ride = RideFactoryDataTest.getRide();
        User passenger = UserFactoryDataTest.getUser();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        Long rideId = 1L;
        passenger.setId(1L);
        ride.getRouteByRouteId().setDriverId(2L);
        ride.setAvailableSeats(0);

        // When
        when(ridePersistencePort.getRideById(rideId)).thenReturn(ride);
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(passenger);

        Throwable throwable = catchThrowable(() -> passengerUseCase.joinRide(rideId, currentUser));

        // Then
        assertThat(throwable).isExactlyInstanceOf(NoMoreSeatsAvailableException.class);
    }

    @Test
    void should_leave_route_given_valid_request() {
        // Given
        User passenger = UserFactoryDataTest.getUser();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        Long rideId = 1L;
        passenger.setRideId(rideId);

        // When
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(passenger);
        passengerUseCase.leaveRoute(currentUser);

        // Then
        verify(userPersistencePort).leaveRide(passenger);
        verify(ridePersistencePort).incrementAvailableSeats(rideId);
        verify(ridePersistencePort).decrementStops(rideId);
    }

    @Test
    void should_throw_exception_user_has_no_ride() {
        // Given
        User passenger = UserFactoryDataTest.getUser();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();
        passenger.setRideId(null);

        // When
        when(userPersistencePort.getUserByUsername(currentUser.getUsername())).thenReturn(passenger);

        Throwable throwable = catchThrowable(() -> passengerUseCase.leaveRoute(currentUser));

        // Then
        assertThat(throwable).isExactlyInstanceOf(PassengerHasNoRideException.class);
    }

    @Test
    void should_get_available_rides_given_valid_request() {
        // Given
        AvailableRide availableRide = RideFactoryDataTest.getAvailableRide();
        CurrentUser currentUser = UserFactoryDataTest.getCurrentUser();

        // When
        passengerUseCase.getAvailableRides(currentUser, availableRide);

        // Then
        verify(neighborhoodPersistencePort).validateIds(anyList());
        verify(ridePersistencePort).getAllAvailableRides(availableRide);
    }
}