package com.davidsanchez.pragpooling.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NeighborhoodTest {

    @Test
    void mustSetAndGetId() {
        Neighborhood neighborhood = new Neighborhood();
        neighborhood.setId(1L);
        assertEquals(1L, neighborhood.getId());
    }

    @Test
    void mustSetAndGetName() {
        Neighborhood neighborhood = new Neighborhood();
        neighborhood.setName("Name");
        assertEquals("Name", neighborhood.getName());
    }
}